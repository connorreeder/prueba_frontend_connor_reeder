import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { RequestResponse } from '../models/RequestResponse';
import { ArrayValueDetails } from '../models/ArrayValueDetails';
import { simpleSort } from '../models/SimpleSort';

@Component({
  selector: 'app-array',
  templateUrl: './array.component.html',
  styleUrls: ['./array.component.css']
})


/**
 * La vista que se muestra en la pestaña de "Array"
 */
export class ArrayComponent implements OnInit {
  // Propiedad que tiene los valores para la tabla
  tableNums: number[] = [];
  // Propiedad que tiene el metadata de los valores (cantidad, primera pos, ultima pos)
  tableDetails: Map<number,ArrayValueDetails>;
  // Propiedad que tiene la lista de números ordendados para el textbox
  sortedNums: number[] = new Array<number>();
  // Propiedad que nos dice si estamos actualmente bajando/processando datos del endpoint o no
  loading: boolean = false;

  // Propiedades para el mensaje de error
  loadAttemptsRemaining: number = 3;
  errorVisible: boolean = false;
  errorMessage: string = "";

  constructor(private _http: HttpService) { }

  ngOnInit() {

  }

  /**
   * Método que se utiliza al apretar el botón
   * Es decir, inicia el proceso de bajar y mostrar los datos
   */
  pullData() {
    // Vaciar la tabla si es necesario
    this.tableNums = [];
    this.loading = true;
    this.hideError();
    // Conectar a endpoint usando el servicio http
    this._http.connectToArrayEndpoint().subscribe(
      response => {
        if (!response) {
          this.showError("La respuesta era inválida o nula. Vuelva a intentar más tarde.");
        } else if (response.error) {
          this.showError("El EndPoint reportó un error. Vuelva a intentar más tarde.");
        } else {
        // Respuesta se recibió correctamente así que lo guardamos en tableData
          this.processTableData(response);
          this.hideError();
          this.loadAttemptsRemaining = 3;
        }
      },
      error => {
        // Hubo un error con la conexión así que después de tres intentos más, simplemente mostramos un error
        if (this.loadAttemptsRemaining > 0) {
          this.loadAttemptsRemaining--;
          this.pullData();
        } else {
          this.loadAttemptsRemaining = 3;
          this.showError("Los datos no se pudieron bajar. Vuelva a intenter cuando tenga mejor conexión.");
        }
      });

  }
  /**
   * Un método que muestra un error
   * @param message Lo que debería decir el error
   */
  private showError(message: string) {
    this.errorVisible = true;
    this.errorMessage = message;
    this.loading = false;
  }
  /**
   * Un método para esconder un error visible
   */
  private hideError() {
    this.errorVisible = false;
  }

  /**
   * Método privado que sirve para procesar los datos del endpoint con el fin de presentarlos en la tabla
   * @param data Respuesta del servidor que hay que procesar
   */
  private processTableData(response: RequestResponse) {
    // Map que contiene los datos(detalles) de todos los valores en el array
    let details = new Map<number, ArrayValueDetails>();
    // Lista que contiene todos los valores en el array
    let nums = [];
    // Iterar por todos los elementos en el array de la respuesta
    for (let i = 0; i < response.data.length; i++) {
      /* Si no es la primera ocurrencia del valor, entonces simplemente actualizamos los detalles de ese valor
      es decir, marcamos la última posición del valor como la actual y incrementamos la cantidad. */
      if (details.has(response.data[i])) {
        details.get(response.data[i]).quantity++;
        details.get(response.data[i]).lastPos = i;

      } else {
        /* Si ES la primera ocurrencia del valor, entonces hay que guardarlo en la lista de nums
        y crearle un objecto de detalles */
        nums.push(response.data[i]);
        details.set(response.data[i], new ArrayValueDetails(1, i, i));
      }
    }
    this.tableDetails = details;
    this.tableNums = nums;
    this.loading = false;
    this.sortedNums = simpleSort(nums);
  }
}
