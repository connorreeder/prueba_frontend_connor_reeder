import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { RequestResponse } from './models/RequestResponse';
import { TextDict } from './models/TextDict';

@Injectable({
  providedIn: 'root'
})

/**
 * Un servico que se comparte entre las dos partes de la aplicación para acceder a endpoints distintos
 */
export class HttpService {

  constructor(private http: HttpClient) { }

  /**
   * Conector al endpoint para el primer ejercicio
   */
  connectToArrayEndpoint() {
    return this.http.get<RequestResponse>("https://patovega.com/prueba_frontend/array.php");
  }

  /**
   * Conector al endpoint para el segundo ejercicio
   */
  connectToDictEndpoint() {
    return this.http.get<RequestResponse>("https://patovega.com/prueba_frontend/dict.php");
  }
}
