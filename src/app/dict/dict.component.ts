import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { RequestResponse } from '../models/RequestResponse';
import { TextDict } from '../models/TextDict';

@Component({
  selector: 'app-dict',
  templateUrl: './dict.component.html',
  styleUrls: ['./dict.component.css']
})
export class DictComponent implements OnInit {

  // los datos consumidos del EndPoint
  dictList: TextDict[] = [];
  // Las cantidades de todas las letras en todos los parrafos
  letterCountRows: number[][];
  // Las sumas de todos los valores numericos de todos los parrafos (para la última columna)
  sums: Array<number> = new Array<number>();
  // Un array que tiene los nombres de las columnas de las letras
  alphabet = Array.from("abcdefghijklmnopqrstuvwxyz");
  // Propiedad que nos dice si estamos actualmente bajando/processando datos del endpoint o no
  loading: boolean = false;

  // Propiedades para el mensaje de error
  loadAttemptsRemaining: number = 3;
  errorVisible: boolean = false;
  errorMessage: string = "";

  constructor(private _http: HttpService) { }

  ngOnInit() {
  }

  /**
   * Método que se utiliza al apretar el botón
   * Es decir, inicia el proceso de bajar y mostrar los datos
   */
  pullData() {
    // Vaciar la tabla si es necesario
    this.dictList = [];
    this.loading = true;
    this.hideError();

    // Conectar a endpoint usando el servicio http
    this._http.connectToDictEndpoint().subscribe(
      response => {
        if (!response) {
          this.showError("La respuesta era inválida o nula. Vuelva a intentar más tarde.");
        } else if (response.error) {
          this.showError("El EndPoint reportó un error. Vuelva a intentar más tarde.");
        } else {
          // Respuesta se recibió correctamente así que lo guardamos en tableData
          this.processTableData(response);
          this.hideError();
          this.loadAttemptsRemaining = 3;
        }
      },
      error => {
        // Hubo un error con la conexión así que después de tres intentos más, simplemente mostramos un error
        if (this.loadAttemptsRemaining > 0) {
          this.loadAttemptsRemaining--;
          this.pullData();
        } else {
          this.loadAttemptsRemaining = 3;
          this.showError("Los datos no se pudieron bajar. Vuelva a intenter cuando tenga mejor conexión.");
        }
      });
  }

  /**
   * Método privado que sirve para procesar los datos del endpoint con el fin de presentarlos en la tabla
   * @param data Respuesta del servidor que hay que procesar
   */
  private processTableData(data: RequestResponse) {
    const dictList: TextDict[] = JSON.parse(data.data);
    // Los códigos unicode para a y z (para establecer rango de caracteres que son letras)
    const aCode: number = "a".charCodeAt(0);
    const zCode: number = "z".charCodeAt(0);
    const code0: number = "0".charCodeAt(0);
    const code9: number = "9".charCodeAt(0);
    // Un array local que contiene todas las listas de cantidades de letras
    // Una vez que hayamos dejado de llenar esta variable, se copia a this.letterCountRows para uso en la vista
    let letterCountRows: number[][] = [];

    let sums: Array<number> = new Array<number>(dictList.length);

    // Por cada dict en la lista
    for (let i = 0; i < dictList.length; i++) {
      // Una variable local que contiene el valor de la llave "paragraph" para el elemento actual
      let paragraph: string = dictList[i].paragraph.toLowerCase();
      // Una lista que contiene las cantidades de todas las letras en este "paragraph"
      let letterCounts: Array<number> = new Array<number>(this.alphabet.length);
      // Llenar la lista con ceros para empezar 
      letterCounts = letterCounts.fill(0);
      // la suma de todos los valores numericos absolutos en este párrafo
      let sum: number = 0;

      /**
       * Por cada carácter en el string con la llave "paragraph"
       */
      // esta variable vamos acumulando digitos para formar un número antes de agregarlo a la variable sum
      let digitos: string = "";
      for (let j = 0; j < paragraph.length; j++) {
        // codigo unicode para el carácter actual
        const charCode = paragraph.charCodeAt(j);

        // Primero revisamos si es un dígito => Si lo es, lo agregamos al string de digitos
        if (code9 >= charCode && charCode >= code0) {
          digitos = digitos + paragraph.charAt(j);

          // Si NO lo es, entonces ha terminado el string de digitos
          // Eso significa que hay que procesarlo y agregarlo a la suma para este párrafo
        } else {

          let parsed = parseInt(digitos);
          if (!isNaN(parsed)) {
            sum += parsed;
          }
          digitos = "";

        }
        // Segundo revisamos si es una letra => Si lo es, aumentamos la cuenta de esa letra
        if (zCode >= charCode && charCode>= aCode) {
          letterCounts[charCode - aCode]++;

        }         

      }
      letterCountRows[i] = letterCounts;
      sums[i] = sum;
    } // Fin de párrafo

    // Cambiar las variables para desencadenar cambios en la vista
    this.letterCountRows = letterCountRows;
    this.sums = sums;
    this.dictList = dictList;
    this.loading = false;
  }
  /**
   * Un método que muestra un error
   * @param message Lo que debería decir el error
   */
  private showError(message: string) {
    this.errorVisible = true;
    this.errorMessage = message;
    this.loading = false;
  }
  /**
   * Un método para esconder un error visible
   */
  private hideError() {
    this.errorVisible = false;
  }
}
