/**
 * Una interfaz que representa un dict que se recibe del endpoint
 */
export interface TextDict {
    paragraph: string,
    number: number,
    hasCopyright: boolean
}