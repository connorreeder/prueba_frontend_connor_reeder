/**
 * Una interfaz que representa la respuesta de un endpoint en esta prueba
 */
export interface RequestResponse {
    data: any,
    error: any,
    success: boolean
}