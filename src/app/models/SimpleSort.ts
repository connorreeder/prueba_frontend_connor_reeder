/**
* una función simple para ordener un arreglo chico
* Sirve para cantidades chicas (por ejemplo < 100)
* pero en el caso de arreglo mucho más grande, habría que usar
* un sort más eficiente (como MergeSort que tiene un rendimiento de O(n log n) en el peor caso)
*/
export function simpleSort(array: number[]): number[] {
  // Clonar el arreglo
  let output = [...array];
  let done = false;
  
  while (!done) {
    done = true;
    // Pasar por todo el arreglo
    for (let i = 1; i < output.length; i++) {
      if (output[i - 1] > output[i]) {
        done = false;
        // Si el valor encontrado el mayor al otro, los cambiamos de posición
        var tmp = output[i - 1];
        output[i - 1] = output[i];
        output[i] = tmp;
      }
    }
  }
  return output;
}