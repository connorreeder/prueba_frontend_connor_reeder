/**
 * Una clase que representa el metadata de un número en un arreglo
 */
export class ArrayValueDetails {
    /**
     * @param quantity Cuántas veces aparece el número en el arreglo
     * @param firstPos la primera posición(indice) del número dentro del arreglo
     * @param lastPos la última posición(indice) del número dentro del arreglo
     */
    constructor(public quantity: number, public firstPos: number, public lastPos: number) {}
}